import React, { Component } from "react";

function todoButtons(props) {
  const { activeCountStatus, handleClearCompleted } = props;
  return (
    <div className="todo-function-keys theme">
      <a className="todo-active-counter">{activeCountStatus} items left</a>
      <div>
        {props.displayTodoArray.map(({ action }) => {
          return (
            <a
              key={action}
              className="todo-main-center-keys"
              onClick={() => props.handleDisplayActions(action)}
            >
              {action}
            </a>
          );
        })}
      </div>
      <a className="todo-clear-completed-key" onClick={handleClearCompleted}>
        CLear Completed
      </a>
    </div>
  );
}

export default todoButtons;
