import React, { Component } from "react";

class todoInputElement extends Component {
  
  state = {
    userInput: ''
  }

  handleUserInput = (event) =>{
    this.setState({
      userInput: event.target.value
    })
  }

  handleSubmit =(event)=>{
    event.preventDefault();
    this.props.addTodo(this.state.userInput);
    this.setState({
      userInput: ''
    })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="todo-input-section-container">
        <div className="todo-input-button-container theme">
          <button className="todo-input-button theme"></button>
        </div>
        <input
          className="todo-input-element theme"
          type="text"
          placeholder="Create a new todo..."

          onChange={this.handleUserInput}
          value={this.state.userInput}
        />
      </form>
    );
  }
}

export default todoInputElement;
