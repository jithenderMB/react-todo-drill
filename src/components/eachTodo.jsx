import React, { Component } from "react";

class eachTodo extends Component {
  render() {
    const {todoDetails} = this.props;
    const {id, content} = todoDetails;
    return (
      <li className={this.handleTextStyle()}>
        <input
          className={this.handleInputStyle()}
          type="checkbox"
          onChange={() => this.props.todoCheckedToggle(id)}
        />
        {content}
      </li>
    );
  }
  handleTextStyle() {
    let elementClass;
    if (this.props.theme === "dark") {
      elementClass = this.props.todoDetails.isChecked
        ? "each-todo-container theme todo-completed-text-style"
        : "each-todo-container theme";
    } else {
      elementClass = this.props.todoDetails.isChecked
        ? "each-todo-container lightTheme theme todo-completed-text-style"
        : "each-todo-container lightTheme theme";
    }
    return elementClass;
  }

  handleInputStyle() {
    let elementClass;
    elementClass = this.props.todoDetails.isChecked
      ? "each-todo-checkbox theme todo-when-clicked"
      : "each-todo-checkbox theme";
    if (this.props.theme === "dark") {
      elementClass = this.props.todoDetails.isChecked
        ? "each-todo-checkbox theme todo-when-clicked"
        : "each-todo-checkbox theme";
    } else {
      elementClass = this.props.todoDetails.isChecked
        ? "each-todo-checkbox theme lightTheme todo-when-clicked"
        : "each-todo-checkbox theme lightTheme";
    }

    return elementClass;
  }
}

export default eachTodo;
