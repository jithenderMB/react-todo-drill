import React, { Component } from "react";
import "../styles/todomain.css";
import EachTodo from "./eachTodo";
import TodoButtons from "./todoButtons";
import TodoInputElement from "./todoInputElement";

const displayTodo = [
  {
    action: "All",
  },
  {
    action: "Active",
  },
  {
    action: "Completed",
  },
];

class todoMain extends Component {
  state = {
    todos: [],
    idGenerator: 1,
    theme: "dark",
    displayAction: "All",
  };

  refreshStateUsingSetStatue = () => {
    this.setState({
      todos: this.state.todos,
    });
  };



  addTodoItem = (userInput) => {
 
      let todoObject = {
        id: this.state.idGenerator,
        content: userInput,
        isChecked: false
      };

      this.setState((prevState) => ({
        todos: [...prevState.todos, todoObject],
        idGenerator: prevState.idGenerator + 1,
      }));
  };


  handleCount = () => {
    return this.state.todos.filter((todo) => !todo.isChecked).length;
  };

  handleCLearCompleted = () => {
    let newArray = [...this.state.todos];
    let filteredArray = newArray.filter((todo) => !todo.isChecked);

    this.setState({
      todos: filteredArray,
    });
  };

  themeChanger = () => {
    this.state.theme = this.state.theme === "dark" ? "light" : "dark";
    Array.from(document.querySelectorAll(".theme")).map((element) =>
      element.classList.toggle("lightTheme")
    );
  };
  changeTheme = (event) => {
    this.themeChanger();

    if (event.target.dataset.typeTheme === "dark") {
      event.target.classList.toggle("hide-image");
      event.target.nextSibling.classList.toggle("hide-image");
    } else if (event.target.dataset.typeTheme === "light") {
      event.target.classList.toggle("hide-image");
      event.target.previousSibling.classList.toggle("hide-image");
    }

    this.refreshStateUsingSetStatue();
  };

  handleTodoToDisplay = () => {
    const {todos, displayAction} = this.state;

    if(displayAction === 'All'){
      return todos
    }
    else if(displayAction === 'Active'){
      return todos.filter(todo => !todo.isChecked)

    }
    else if(displayAction === 'Completed'){
      return todos.filter(todo => todo.isChecked)

    }
  }
  handleTasksToDisplay = (action) => {
    this.setState({
      displayAction: action,
    });
  };

  handleTodoCheckedStatus = (id) => {
    this.setState({
      todos: this.state.todos.map((todo) => {
        if (todo.id === id) {
          return {
            ...todo,
            isChecked: !todo.isChecked,
          };
        }
        return todo;
      }),
    });
  };

  render() {
    const todosArray = this.handleTodoToDisplay();
    return (
      <main className="main theme">
        <div className="todo-bg-image">
          <div className="todo-elements-conatiner">
            <div className="todo-title-theme-icon-container">
              <h1 className="todo-main-title">TODO</h1>
              <div onClick={(event) => this.changeTheme(event)}>
                <img
                  className="theme-changer-image"
                  data-type-theme="dark"
                  src="./images/icon-sun.svg"
                  alt="sunLogo.jpg"
                />
                <img
                  className="theme-changer-image hide-image"
                  data-type-theme="light"
                  src="./images/icon-moon.svg"
                  alt="moonLogo.jpg"
                />
              </div>
            </div>

            <TodoInputElement addTodo={this.addTodoItem} />

            <ul className="todo-list-container theme">
              {
                todosArray
                .map((todo) => (
                  <EachTodo
                    key={todo.id}
                    todoCheckedToggle ={this.handleTodoCheckedStatus}
                    todoDetails={todo}
                    theme={this.state.theme}
                  />
                ))}
            </ul>

            <TodoButtons
              activeCountStatus={this.handleCount()}
              displayTodoArray = {displayTodo}
              handleClearCompleted={this.handleCLearCompleted}
              handleDisplayActions = {this.handleTasksToDisplay}
            />

            <p className="drag-drop-text">Drag and drop to reader list</p>
          </div>
        </div>
      </main>
    );
  }
}

export default todoMain;
